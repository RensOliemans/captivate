# SPDX-FileCopyrightText: 2022 Rens Oliemans <hallo AT rensoliemans.nl>
# SPDX-License-Identifier: GPL-3.0-or-later


MODULE = menumaster@rensoliemans.nl
INSTALLDIR =~/.local/share/gnome-shell/extensions/
INSTALLPATH=~/.local/share/gnome-shell/extensions/$(MODULE)

all: lint build

.PHONY: lint

build:
	glib-compile-schemas --strict $(MODULE)/schemas/

install: all
	mkdir -p $(INSTALLPATH)
	cp -r $(MODULE) $(INSTALLDIR)
uninstall:
	rm -r $(INSTALLPATH)
update: uninstall install

lint:
	eslint .

bundle: all
	cd $(MODULE); zip -FS -r ../bundle.zip * -x \*.license
