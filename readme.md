<!--
SPDX-FileCopyrightText: 2022 Rens Oliemans <hallo AT rensoliemans.nl>
SPDX-License-Identifier: GPL-3.0-or-later
-->

# MenuMaster

The goal of this extension is to make the [Menu
key](https://en.wikipedia.org/wiki/Menu_key) useful. You can choose what the
Menu key does (focus a chosen window, run a script, send the key (or another) to
the window), and what Shift-Menu does (choose a window to focus, or any of the
previously named). Actually, you can choose what button to use, it needn't be
the Menu key.

# Manual install

Do `make install`, and restart your Gnome session (`Alt+F2` -> `r`, or log out
and in again in Wayland). Enable it, and you've got it working.


## Credits

Credits for the getWindowInFocus logic goes to FlorianLudwig, from [this
comment](https://github.com/ActivityWatch/activitywatch/issues/92).

I used
[awesome-tiles](https://github.com/velitasali/gnome-shell-extension-awesome-tiles/)
(commit 409cd39) as example for my `prefs.js`, that was very useful.

I used [blur-my-shell](https://github.com/aunetx/blur-my-shell/) for the handy
settings wrapper, commit f50179e.
