// SPDX-FileCopyrightText: 2022 Rens Oliemans <hallo AT rensoliemans.nl>
// SPDX-License-Identifier: GPL-3.0-or-later
//
// Largely inspired by aunetx/blur-my-shell

const ExtensionUtils = imports.misc.extensionUtils;


var Prefs = class Prefs {
    constructor() {
        this.settings = ExtensionUtils.getSettings();

        this.ACTIVATE = {
            key: 'activate',
            get: () => this.settings.get_strv('activate')[0],
            set: v => this.settings.set_strv('activate', [v]),
            reset: () => this.settings.reset('activate'),
            changed: cb => this.settings.connect('changed::activate', cb),
            disconnect: () => this.settings.disconnect(...this.settings),
        };

        this.SET_ACTIVATE = {
            key: 'set-activate',
            get: () => this.settings.get_strv('set-activate')[0],
            set: v => this.settings.set_strv('set-activate', [v]),
            reset: () => this.settings.reset('set-activate'),
            changed: cb => this.settings.connect('changed::set-activate', cb),
            disconnect: () => this.settings.disconnect(...this.settings),
        };

        this.NOTIFY = {
            key: 'notify',
            get: () => this.settings.get_boolean('notify'),
            set: v => this.settings.set_boolean('notify', v),
            reset: () => this.settings.reset('notify'),
            changed: cb => this.settings.connect('changed::notify', cb),
            disconnect: () => this.settings.disconnect(...this.settings),
        };
    }
};
