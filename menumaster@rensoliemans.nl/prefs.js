// SPDX-FileCopyrightText: 2022 Rens Oliemans <hallo AT rensoliemans.nl>
// SPDX-License-Identifier: GPL-3.0-or-later
//
// Inspired by velitasali/gnome-shell-extension-awesome-tiles

const { Gdk, Gtk, GObject, Gio, Pango } = imports.gi;

const ExtensionUtils = imports.misc.extensionUtils;
const Me = ExtensionUtils.getCurrentExtension();
const Settings = Me.imports.settings;
const Utils = Me.imports.utils;

const config = new Settings.Prefs();

const ActivatePrefsWidget = GObject.registerClass({
    GTypeName: 'ActivatePrefsWidget',
    Template: Me.dir.get_child('prefs.ui').get_uri(),
    InternalChildren: [
        'notify',
        'activate',
        'setactivate',
    ],
}, class ActivatePrefsWidget extends Gtk.Frame {
    _init() {
        super._init();

        this._notify.set_active(config.NOTIFY.get());
        this._activate.set_label(config.ACTIVATE.get());
        this._setactivate.set_label(config.SET_ACTIVATE.get());

        this.connectSettings();
    }

    notifyChanged(w) {
        let value = w.get_active();
        config.NOTIFY.set(value);
    }

    activateClicked() {
        const dialog = new ActivateShortcutDialog(config.ACTIVATE);
        dialog.set_transient_for(this.get_root());
        dialog.present();
    }

    setActivateClicked() {
        this._showDialog(config.SET_ACTIVATE);
    }

    _showDialog(key) {
        const dialog = new ActivateShortcutDialog(key);
        dialog.set_transient_for(this.get_root());
        dialog.present();
    }

    connectSettings() {
        config.NOTIFY.changed(_ => this._notify.set_active(config.NOTIFY.get()));
        config.ACTIVATE.changed(_ => this._activate.set_label(config.ACTIVATE.get()));
        config.SET_ACTIVATE.changed(_ => this._setactivate.set_label(config.SET_ACTIVATE.get()));
    }
});

const ActivateShortcutDialog = GObject.registerClass({
    GTypeName: 'ActivateShortcutDialog',
    Template: Me.dir.get_child('prefs-shortcut-dialog.ui').get_uri(),
    InternalChildren: [
        'event-controller',
    ],
}, class ActivateShortcutDialog extends Gtk.Dialog {
    _init(setting) {
        super._init();
        this.setting = setting;

        this._event_controller.connect('key-pressed', (w, kv, kc, s) => this.keyPressed(w, kv, kc, s));
    }

    keyPressed(widget, keyval, keycode, state) {
        let mask = state & Gtk.accelerator_get_default_mod_mask();
        mask &= ~Gdk.ModifierType.LOCK_MASK;

        if (mask === 0 && keyval === Gdk.KEY_Escape) {
            this.visible = false;
            return Gdk.EVENT_STOP;
        } else if (mask === 0 && keyval === Gdk.KEY_BackSpace) {
            this.visible = false;
            this.setting.reset();
            return Gdk.EVENT_STOP;
        }

        if (!Utils.isBindingValid({ mask, keycode, keyval }) || !Utils.isAccelValid({ mask, keyval }))
            return Gdk.EVENT_STOP;


        const binding = Gtk.accelerator_name_with_keycode(
            null,
            keyval,
            keycode,
            mask
        );

        this.setting.set(binding);
        this.visible = false;
        return Gdk.EVENT_STOP;
    }
});

function init() { }


function buildPrefsWidget() {
    return new ActivatePrefsWidget();
}
