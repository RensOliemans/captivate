// SPDX-FileCopyrightText: 2022 Rens Oliemans <hallo AT rensoliemans.nl>
// SPDX-License-Identifier: GPL-3.0-or-later

'use strict';

const { Shell, Meta } = imports.gi;
const Main = imports.ui.main;
const MessageTray = imports.ui.messageTray;

const ExtensionUtils = imports.misc.extensionUtils;
const Me = ExtensionUtils.getCurrentExtension();
const Settings = Me.imports.settings;
const Utils = Me.imports.utils;

const ICON = 'view-mirror-symbolic';

// Settings
let settings;
let NOTIFY = true;

let activeWindow;
let notifySource;
let handlerId;

function init() { }

function enable() {
    let mode = Shell.ActionMode.NORMAL;
    let flag = Meta.KeyBindingFlags.NONE;
    settings = new Settings.Prefs();
    loadSettings();

    Main.wm.addKeybinding('activate', settings.settings, flag, mode, focusActivatedWindow);
    Main.wm.addKeybinding('set-activate', settings.settings, flag, mode, setActivate);
}

function disable() {
    Main.wm.removeKeybinding('activate');
    Main.wm.removeKeybinding('set-activate');

    destroy(activeWindow);
    destroy(notifySource);
    disconnect();
    destroy(settings);
}

function destroy(obj) {
    if (obj) {
        if (obj.destroy)
            obj.destroy();
        obj = null;
    }
}

function disconnect() {
    if (handlerId) {
        settings.settings.disconnect(handlerId);
        handlerId = null;
    }
}

function loadSettings() {
    handlerId = settings.settings.connect('changed', fetchSettings);
    fetchSettings();
}

function fetchSettings() {
    NOTIFY = settings.NOTIFY.get();
}

function focusActivatedWindow() {
    let metaWindows = getMetaWindows();
    let active = metaWindows.find(mw => mw === activeWindow);

    if (!active) {
        setActivate();
        return;
    }

    let focused = getWindowInFocus();
    if (active === focused)
        focusWindow(getPreviouslySelected(active));
    else
        focusWindow(active);
}

function getPreviouslySelected(currentlySelected) {
    let windows = getMetaWindows().filter(mw => mw !== currentlySelected);
    windows.sort(Utils.SortWindowsByUserTime);
    if (windows.length >= 1)
        return windows[0];
    log('No previously selected window exists, not switching');
    return false;
}

function focusWindow(window) {
    if (window) {
        const workspace = window.get_workspace();
        workspace.activate_with_focus(window, 0);
    }
}

function setActivate() {
    const inFocus = getWindowInFocus();
    activeWindow = inFocus;
    showNotificationIfEnabled(inFocus);
}

function getWindowInFocus() {
    let metaWindows = getMetaWindows();
    return metaWindows.find(mw => mw.has_focus());
}

function getMetaWindows() {
    return global.get_window_actors().map(w => w.meta_window);
}

function showNotificationIfEnabled(newActiveWindow) {
    if (NOTIFY)
        showNotification(newActiveWindow);
}

function showNotification(newActiveWindow) {
    initNotifySource();

    const message = `Set ${newActiveWindow.get_wm_class()} as active.`;

    let notification = null;
    if (notifySource.count === 0) {
        notification = new MessageTray.Notification(notifySource, message);
    } else {
        // We're already showing a notification. Update that one instead of
        // creating a new one.
        notification = notifySource.notifications[0];
        notification.update(message, '', { clear: true });
    }

    notification.setTransient(true);
    notifySource.showNotification(notification);
}

function initNotifySource() {
    if (!notifySource) {
        notifySource = new MessageTray.Source('ClipboardIndicator', ICON);
        notifySource.connect('destroy', () => {
            notifySource = null;
        });

        Main.messageTray.add(notifySource);
    }
}
